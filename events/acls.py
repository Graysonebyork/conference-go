from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json, requests

def get_photo(query):

    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={query}"
    response = requests.get(url, headers=headers)
    api_dic = response.json()
    photo_url = api_dic["photos"][0]["src"]["original"]

    return photo_url

def get_weather_data(city, state):

    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    #temp_url = f"http://api.openweathermap.org/geo/1.0/direct?q=London,England,GB&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(geo_url)
    location = {
        "lon": response.json()[0]["lon"],
        "lat": response.json()[0]["lat"]
    }
    lon = location["lon"]
    lat = location["lat"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    weather_response = requests.get(weather_url)
    weather = {
        "main": weather_response.json()["weather"][0]["main"],
        "description": weather_response.json()["weather"][0]["description"],
        "temperature": weather_response.json()["main"]["temp"]
    }

    return weather
    pass


    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary



    #https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}
#https://api.openweathermap.org/data/2.5/weather?lat=32.7762719&lon=-96.7968559&appid=dbf275b52453bc644f41aba6b5e016a5
