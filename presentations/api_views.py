from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from events.models import Conference
from django.views.decorators.http import require_http_methods
import json

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        p = Presentation.objects.all()
        return JsonResponse(
            p,
            encoder=PresentationListEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):

    if request.method == "GET":
        p = Presentation.objects.get(id=id)
        return JsonResponse(
            p,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

    elif request.method == "PUT":
        content = json.loads(request.body)

        Presentation.objects.filter(id=id).update(**content)

        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
